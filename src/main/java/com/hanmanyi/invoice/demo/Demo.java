package com.hanmanyi.invoice.demo;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.hanmanyi.invoice.db.entity.Detail;
import com.hanmanyi.invoice.db.entity.Detail1;
import com.hanmanyi.invoice.db.entity.Invoice;
import com.hanmanyi.invoice.service.InvoiceData;
import com.hanmanyi.invoice.service.InvoiceExtractor;

/**
 * 样例：调用方式、解析结果、字段说明
 * 
 * @author 皮球爸爸
 *
 */
public class Demo {
	public static void main(String[] args) {
		InvoiceExtractor invoiceExtractor= new InvoiceExtractor();
    	String filename = "C:\\Users\\Administrator\\Desktop\\123.pdf";
    	try {
			InvoiceData data= invoiceExtractor.extract(filename);
			
			Invoice invoice = data.getInvoice();//识别内容
			JSONObject json=(JSONObject) JSONObject.toJSON(invoice);
			System.out.println("解析结果：");
			System.out.println(json);
			
			//货物明细
			if(data.getDetail() != null){
				for(Object detail : data.getDetail()){
					if(data.getType().equals("00")){
						//普通发票
						Detail d = (Detail)detail;
						System.out.println((JSONObject) JSONObject.toJSON(d));
					}else{
						//通行费发票
						Detail1 d1 = (Detail1)detail;
						System.out.println((JSONObject) JSONObject.toJSON(d1));
					}
				}
			}
			
			Map<String,Object> info = new HashMap<String,Object>();
			System.out.println("属性含义：");
			info.put("nsrsbh2", "纳税人识别号（销售方）");
			info.put("fh", "复核");
			info.put("nsrsbh1", "纳税人识别号（购买方）");
			info.put("title", "标题");
			info.put("type", "发票类型");
			info.put("hjje", "合计金额");
			info.put("mmq", "密码区");
			info.put("file", "文件路径");
			info.put("kprq", "开票日期");
			info.put("khhjzh1", "开户行及账号（购买方）");
			info.put("mc1", "名称（购买方）");
			info.put("khhjzh2", "开户行及账号（销售方）");
			info.put("skr", "收款人");
			info.put("mc2", "名称（销售方）");
			info.put("dzdh2", "地址、电话（销售方）");
			info.put("fpdm", "发票代码");
			info.put("hjse", "合计税额");
			info.put("jshjxx", "价税合计（小写）");
			info.put("kpr", "开票人");
			info.put("uploadTime", "解析时间");
			info.put("dzdh1", "地址、电话（购买方）");
			info.put("jshjdx", "价税合计（大写）");
			info.put("jqbh", "机器编号");
			info.put("fphm", "发票号码");
			info.put("jym", "校验码");
			System.out.println(info);
			System.out.println("解析完成");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
